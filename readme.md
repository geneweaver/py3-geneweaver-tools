
GeneWeaver 2.0 Tool Setup and Deployment
========================================

Documentation for configuring and deploying the GeneWeaver toolset on a
compute node.


## System Requirements

GeneWeaver is designed to run on Linux and requires a relatively recent
release. It has been tested on CentOS 6/7 and Red Hat distributions but should
run on other distributions with minimal changes. 

To begin, you will need the following application dependencies:

RedHat/Fedora/CentOS:

    $ sudo yum install boost boost-devel git graphviz libffi libffi-devel libpqxx libpqxx-devel postgresql-server postgresql-devel python2 rabbitmq-server supervisor
    
Debian/Ubuntu:

    $ sudo apt-get install libboost-all-dev git graphviz libffi6 libffi-dev libpqxx-4.0 libpqxx-dev postgresql postgresql-server-dev-9.5 python2.7 rabbitmq-server supervisor

Ensure that the following applications meet these version requirements:

* python == __2.7.*__
* PostgreSQL >= __9.2__
* Graphviz >= __2.3__
* RabbitMQ >= __3.3.*__

Retrieve the GeneWeaver toolset from the BitBucket repo.
Create a new project folder if you haven't already:

    $ mkdir -p /srv/geneweaver && cd /srv/geneweaver
    $ git clone https://YOUR_USERNAME@bitbucket.org/geneweaver/tools.git


## Configuring the Python Environment

The python package manager pip, should be included by default in Python 2.7.
If it is missing from your system, bootstrap the installation by downloading 
and executing the pip installer:

    $ wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py


### Configuring virtualenv (optional)

Installing virtualenv is optional but it creates an easy to use, stand-alone
environment in which to install python packages and set up the application.
Installation may require sudo privileges. Use pip to install:

    $ pip install virtualenv

Create the GeneWeaver virtual environment. In this example the GeneWeaver
application will reside under `/srv`:

    $ mkdir /srv/geneweaver && cd /srv/geneweaver
    $ virtualenv venv

Activate the environment:

    $ source venv/bin/activate

You can now install any python packages without contaminating the global python
environment. To deactivate the environment use:

    $ deactivate


### Installing Packages

The toolset requires the following python packages:

* celery
* numpy
* psycopg2
* numpy

These can be installed individually using pip (which should also install their
dependencies) or using our `requirements.txt` file.
Installation of these packages may require sudo privileges. 
The requirements file can be found in the `tools/docs` directory.

    $ pip install -r requirements.txt


## Configuring the Toolset

First edit `tools/config.py` and change the `CONFIG_PATH` variable to point to
a location to store the tool config file. 
For the rest of this documentation, `/srv/geneweaver` will be used as the base
directory.
From the tools parent directory (in this case `/srv/geneweaver`), you can run 
the tools once to generate a default config.
If you installed virtualenv, be sure to run the tools from that environment.

    $ celery -A tools.celeryapp worker --loglevel=info

This will generate a sample config file for you at `/srv/geneweaver/tools.cfg`.
Edit the config with the appropriate information for your setup.

**Note 1.** If using multiple compute nodes, the results directory should 
be a directory that can be shared between all compute nodes and the server 
running the web application.

**Note 2.** If using multiple compute nodes, you should edit the `url` field in
`tools.cfg` to point to the web server, which should also be running RabbitMQ
and will distribute runs across each compute node.
The url field should be in the format `amqp://guest@WEB-SERVER//`

**Note 3.** The tool table may be incompatible with the celeryapp.py tool list.
You may drop the tool table data and reload with ODE-data-only-tool.dump to 
correct.

If you need to upgrade an older version of the toolset running celery 3.x,
follow these steps:

1. Pull the latest version of the `tools` and `website-py` repos.
2. Upgrade the package requirements using pip `$ pip install -r website-py/sample-configs/requirements.txt`.
3. Restart the tool and web applications.


#### Compiling the Graph Tools

We use several highly optimized software implementations written in C/C++.
This suite of tools can be found in the `TOOLBOX/` directory and should be
compiled prior to running the toolset written in Python. 

Ensure gcc and other development tools exist on your system. If they are
missing, install them:

    $ sudo yum install gcc g++ make

These tools can be compiled using the "master" makefile located in TOOLBOX.

    $ cd tools/TOOLBOX && make && cd ../..


#### Compiling the Distribution Generator

The distribution generator tool is written in C++ and used to generate a null 
distribution with which we can use to assess the significance of a jaccard 
similarity result. 
It is located in the `tools/cpp_tools` directory. 
This tool requires two dependencies, `libpqxx` and `libpqxx-devel` which 
should have been installed earlier. 

This tool requires a connection to the database. 
Unless you are using a different installation directory or database 
credentials, the only connection variable you should have to change is the 
database host address. 
This must be changed in the following files: `distribution_generator.cpp`, 
`drone.cpp`, and `fileGenerator.cpp`. 
`fileGenerator.cpp` contains two separate lines where the host address must be changed.

To change all the necessary lines in a single sitting, run the following command in the tools directory:

    $ cd tools
    $ find . -name "*.cpp" -exec sed -i "s/127.0.0.1/DATABASE_IP/g" '{}' \;

Depending on your setup, you may also have to change the DB connection
credentials which can be found around lines 370, 313, and 75 of the
`distribution_generator.cpp`, `drone.cpp`, and `fileGenerator.cpp` files
respectively.
You may also need to change the locations of the gene and homology data files
required by the generator.
These are found around lines 386 and 406 of `distribution_generator.cpp` and
lines 32 and 68 of `fileGenerator.cpp`.

Finally, compile the distribution generator:

    $ cd TOOLBOX/distribution_generator && make


## Managing GeneWeaver with Supervisor

Supervisor is a system management utility that can be used to manage the tools 
and message broker (RabbitMQ).
It is recommended to use supervisor rather than starting the software manually.
For manual instructions, see the sections below.
Supervisor should already be installed but if it is missing, use the following
command to install it:

    $ sudo yum install supervisor

Copy the sample supervisord config from the `tools/docs` directory to one of
your choosing. 
Here we use the geneweaver application directory:

    $ cp sample-configs/supervisord.conf /srv/geneweaver

The supervisor config in `tools/docs` is a stripped down version of the one
found in `website-py/sample-configs`.
It is designed for use with compute nodes only running the toolset.

Create a folder to store the supervisord logs, or store them in any directory
you wish:

    $ mkdir /srv/geneweaver/supervisord

Now edit the `supervisord.conf` file to match your installation and log
directories. 
After editing, you can start supervisor:

    $ sudo supervisord -c /srv/geneweaver/supervisord.conf

To manage your applications use:

    $ sudo supervisorctl -c /srv/geneweaver/supervisord.conf

Start the message broker and tools from the supervisor command prompt:

    $ start rabbitmq
    $ start gw-tools


## Running RabbitMQ Manually

RabbitMQ is the message broker used by Celery to distribute GW tool runs. 
It can be run in the background:

    $ rabbitmq-server start &

Or it can be configured to start on boot and daemonized:

    $ chkconfig rabbitmq-server on
    $ /sbin/service rabbitmq-server start

Or daemonized using systemctl: 

    $ systemctl start rabbitmq-server.service


## Running the Tools Manually

Start the tools application from the parent directory of the tools
(`/srv/geneweaver` throughout this documentation):

    $ celery -A tools.celeryapp worker --loglevel=info



## Configuring the Python3 Environment

Python3 versions and pip packages can be manged by `pipenv`

### How to run project

#### Initialize packages
```bash
$ cd {PROJECT_ROOT}
$ pip install pipenv
$ pipenv sync
```

#### Run packages
```bash
$ pipenv run python -m celery -A tools.celeryapp worker --loglevel=info
```

#### Synchronize packages with current `Pipfile.lock` file
```
$ pipenv sync
```

The more on the `pipenv` [documentation](https://pipenv.kennethreitz.org/en/latest/#install-pipenv-today "documentation").
