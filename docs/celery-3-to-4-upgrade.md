# Upgrading From Celery 3.1.23 to 4.0.2

This is a write up document describing the changes made to the tools.

## 1. [Update the configuration with the new setting names](http://docs.celeryproject.org/en/latest/whatsnew-4.0.html#step-2-update-your-configuration-with-the-new-setting-names)

As specified [here](http://docs.celeryproject.org/en/latest/whatsnew-4.0.html#v400-upgrade-settings), all settings are renamed to be in lowercase.
Celery included a command-line utility to ease this transition:
```
$ celery upgrade settings celeryapp.py
```
Note that this command creates a celeryapp.py.orig backup file that has since been discarded.
Also note that this step is optional; the Celery tools has been tested to work without this change.

## 2. [Manually register tasks](http://docs.celeryproject.org/en/latest/whatsnew-4.0.html#the-task-base-class-no-longer-automatically-register-tasks)

As of 4.0, "The Task class is no longer using a special meta-class that automatically registers the task in the task registry."
Since the tools uses class based tasks, this means that the tasks needs to be registered manually:

1. Each tools file needs to import celery from celeryapp.py:

    ```
    from tools.celeryapp import celery
    ```

2. For each class that is a tool, that class needs a `name` attribute of type `String`:

    ```
    name = "tools.{CLASS_NAME}.{CLASS_NAME}"
    ```

3. Each class needs to be registered to the app (after the class has been defined):

    ```
    {CLASS_NAME} = celery.register_task({CLASS_NAME}())
    ```

Note that `celery` here was imported from celeryapp.py.
Also note that an instance of the class is passed to the register_task function.

These changes have been made to the following classes in their respective files:

1. PhenomeMap
2. GeneSetViewer
3. Combine
4. JaccardSimilarity
5. JaccardClustering
6. BooleanAlgebra
7. TricliqueViewer
8. ABBA
9. DBSCAN
10. MSET

## 3. TODO: other changes that should also be made to the tools before Celery 5.0

1. [Upgrade from Python 2.x to 3.x](http://docs.celeryproject.org/en/latest/whatsnew-4.0.html#last-major-version-to-support-python-2)
2. [Replace using the `amqp` with something else, such as an `rpc`](http://docs.celeryproject.org/en/latest/whatsnew-4.0.html#features-removed-for-lack-of-funding)

